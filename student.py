
class Student:
    def __int__(self,first_name: str,last_name: str,student_number:str,status: bool,grade: list):
        self.__first_name = first_name
        self.__last_name = last_name
        self.__student_number = student_number
        self.__status = status
        self.__grade = grade

    def get_first_name(self) -> str:
        return self.__first_name

    def set_first_name(self,first_name:str):
        return self.__first_name

    def get_last_name(self) -> str:
        return self.__last_name

    def set_last_name(self, last_name: str):
        self.__last_name = last_name

    def get_student_number(self) -> str:
        return self.__last_name

    def check_student_number(self, last_name: str):

        if self.__student_number.startswith('A')==False or len(self.__student_number)>9:
            return False
        else:
            return self.__student_number

    def get_grade(self) -> list:
        return self.__grade

#grades should be between 0 to 100
    def set_grade(self,grade):
        for each_grade in grade:
            if each_grade > 0 and each_grade <101 ==True :
                return "Wrong marks entered"
        self.__grade = grade


    def __str__(self):
        return self.__first_name + " " + self.__last_name + " " + self.__student_number + " " + self.__status + " " + self.__grade